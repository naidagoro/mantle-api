/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

pipeline {
  agent none
  options {
    checkoutToSubdirectory('repo')
    timeout(time: 5, unit: 'HOURS')
    timestamps()
  }
  stages {
    stage('Linux and Windows build') {
      parallel {
        stage('Linux') {
          agent {
            kubernetes {
              label 'openpass-agent-pod-' + env.BUILD_NUMBER
              yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: mantleapi-build
    image: nmraghu/mantleapi:latest
    tty: true
    resources:
      limits:
        memory: "16Gi"
        cpu: "4"
      requests:
        memory: "16Gi"
        cpu: "4"
  - name: jnlp
    volumeMounts:
    - name: volume-known-hosts
      mountPath: /home/jenkins/.ssh
  volumes:
  - name: volume-known-hosts
    configMap:
      name: known-hosts
"""
            }
          }
          stages {
            stage('Linux: Setup environment') {
              steps {
                container('mantleapi-build') {
                  sh 'bash repo/utils/ci/scripts/10_build_prepare.sh'
                  sh 'bash repo/utils/ci/scripts/20_configure.sh'
                }
              }
            }
            stage('Linux: Build MantleAPI') {
              steps {
                container('mantleapi-build') {
                  sh 'bash repo/utils/ci/scripts/30_build.sh'
                }
              }
            }
            stage('Linux: Run Unittest') {
              steps {
                container('mantleapi-build') {
                  sh 'bash repo/utils/ci/scripts/40_test.sh'
                }
              }
            }
          }
        }
        stage('Windows') {
          agent {
            label 'windows'
          }
          environment {
            MSYSTEM = 'MINGW64'
            CHERE_INVOKING = 'yes'
          }
          stages {
            stage('Windows: Setup environment') {
              steps {
                bat 'subst M: %WORKSPACE%'
                dir('M:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/10_build_prepare.sh'
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/20_configure.sh'
                }
              }
            }
            stage('Windows: Build MantleAPI') {
              steps {
                dir('M:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/30_build.sh'
                }
              }
            }
            stage('Windows: Run Unittest') {
              steps {
                dir('M:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/40_test.sh'
                }
              }
            }
          }
          post {
            always {
              bat 'subst M: /d'
            }
          }
        }
      }
    }
  }
}
