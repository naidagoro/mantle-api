/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MantleAPI/Common/i_logger.h"
#include "MantleAPI/Common/mock_logger.h"

namespace
{

using mantle_api::LogLevel;
using testing::_;
using testing::Const;
using testing::Return;

class LoggerTest : public testing::Test
{
protected:
  void SetUp() override
  {
    mock_logger_.DelegateToFake();
  }

  mantle_api::MockLogger mock_logger_;
  mantle_api::ILogger &logger_{mock_logger_};
};

TEST_F(LoggerTest, GetCurrentLogLevel)
{
  EXPECT_CALL(Const(mock_logger_), GetCurrentLogLevel())  //
      .Times(1)                                           //
      .WillRepeatedly(Return(LogLevel::kTrace));
  ASSERT_NO_THROW(std::ignore = logger_.GetCurrentLogLevel());
}

TEST_F(LoggerTest, Log)
{
  EXPECT_CALL(mock_logger_, Log(_, _)).Times(1);
  ASSERT_NO_THROW(logger_.Log({}, "LoggerTest_Log"));
}

TEST_F(LoggerTest, Trace)
{
  EXPECT_CALL(mock_logger_, Log(LogLevel::kTrace, _)).Times(1);
  ASSERT_NO_THROW(logger_.Trace("LoggerTest_Trace"));
}

TEST_F(LoggerTest, Debug)
{
  EXPECT_CALL(mock_logger_, Log(LogLevel::kDebug, _)).Times(1);
  ASSERT_NO_THROW(logger_.Debug("LoggerTest_Debug"));
}

TEST_F(LoggerTest, Info)
{
  EXPECT_CALL(mock_logger_, Log(LogLevel::kInfo, _)).Times(1);
  ASSERT_NO_THROW(logger_.Info("LoggerTest_Info"));
}

TEST_F(LoggerTest, Warning)
{
  EXPECT_CALL(mock_logger_, Log(LogLevel::kWarning, _)).Times(1);
  ASSERT_NO_THROW(logger_.Warning("LoggerTest_Warning"));
}

TEST_F(LoggerTest, Error)
{
  EXPECT_CALL(mock_logger_, Log(LogLevel::kError, _)).Times(1);
  ASSERT_NO_THROW(logger_.Error("LoggerTest_Error"));
}

TEST_F(LoggerTest, Critical)
{
  EXPECT_CALL(mock_logger_, Log(LogLevel::kCritical, _)).Times(1);
  ASSERT_NO_THROW(logger_.Critical("LoggerTest_Critical"));
}

}  // namespace
