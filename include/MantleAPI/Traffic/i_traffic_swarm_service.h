/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_traffic_swarm_service.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_TRAFFIC_SWARM_CONTROLLER_H
#define MANTLEAPI_TRAFFIC_I_TRAFFIC_SWARM_CONTROLLER_H

#include <vector>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Common/pose.h>
#include <units.h>

namespace mantle_api
{

struct TrafficSwarmParameters
{
    std::string central_entity_name;
    size_t maximum_number_of_vehicles;
    units::velocity::meters_per_second_t vehicle_velocity;
    units::length::meter_t exclusion_radius;
    units::length::meter_t semi_minor_spawning_radius;
    units::length::meter_t semi_major_spawning_radius;
    units::length::meter_t spawning_area_longitudinal_offset;
};

class ITrafficSwarmService
{
public:
    virtual std::vector<mantle_api::Pose> GetAvailableSpawningPoses() const = 0;
    virtual mantle_api::VehicleProperties GetVehicleProperties(mantle_api::VehicleClass vehicle_class) const = 0;
    virtual void UpdateControllerParameters(std::unique_ptr<mantle_api::ExternalControllerConfig>& config) = 0;
    virtual void SetSwarmEntitiesCount(size_t count) = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_TRAFFIC_SWARM_CONTROLLER_H
